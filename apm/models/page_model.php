<?php

class Page_model extends CI_Model
{
	private $_table_pages = "pages";

	public $id = 0;
	public $clean_title = '';
	public $title = '';
	public $content = '';
	public $banner = '';

	public function
		__construct			($_data = FALSE)
						{
							if ($_data === FALSE)
								parent::__construct ();
							else
							{
								$this->id = (int) $_data->id;
								$this->clean_title = $_data->clean_title;
								$this->title = $_data->title;
								$this->content = $_data->content;
								$this->banner = $_data->banner;

							}
						}
	public function
		getByID				($ID)
						{
							return $this->get ('id', $ID);
						}
	public function
		getByCleanTitle		($ID)
						{
							return $this->get ('clean_title', $ID);
						}
	private function
		get					($field, $ID)
						{
							$this->db->where ($field, $ID);
							$query = $this->db->get ($this->_table_pages);
							foreach ($query->result () as $data)
								return new Page_model ($data);
							return null;
						}
	public function
		add					()
						{
							$this->db->insert ($this->_table_pages, $this->toArray ());
							$this->id = $this->db->insert_id ();
							return $this;
						}
	public function
		update				()
						{
							$this->db->where ('id', $this->id);
							$this->db->update ($this->_table_pages, $this->toArray ());
							return $this;
						}
	public function
		remove				()
						{
							$this->db->where ('id', $this->id);
							$this->db->delete ($this->_table_pages, array ('id' => $this->id));
						}
	public function
		toArray				()
						{
							return array (
								"clean_title" => $this->clean_title
								, "title" => $this->title
								, "content" => $this->content
								, "banner" => $this->banner
								, "id" => $this->id
							);
						}
	public function
		toJSON				()
						{
							return array (
								"clean_title" => $this->clean_title
								, "title" => $this->title
								, "content" => $this->content
								, "id" => $this->id
								, "banner" => $this->banner
							);
						}
	public function
		toString				()
						{
							return  json_encode ($this->toJSON ());
						}
}