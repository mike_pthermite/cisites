<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width">
		<!-- // TODO: check for CSS3. <IE9, <Firefox 4, <Safari 5. -->
		<title><?php echo $title ?></title>
		<?php foreach($myCSS as $lnk): ?>
		<link rel="stylesheet" type="text/css" href="<?php echo $lnk ?>" /><?php endforeach; ?>
		<?php foreach($myJS as $lnk): ?>
		<script type="text/javascript" src="<?php echo $lnk ?>"></script><?php endforeach; ?>
		<!--[if gte IE 9]>
			<style type="text/css">
				.content {
					filter: none;
				}
			</style>
		<![endif]-->
	<?php if ($admin): ?>
		<link rel="stylesheet" href="<?php echo base_url('/'); ?>css/aloha/aloha-common-extra.css" type="text/css">
	<?php endif; ?>
	</head>
	<body>
		<?php $this->load->view('global.templates.inc'); ?>
		<div id="dialogs"></div>
		<div id="wrapper">
			<div id="header">
				<div id="header-top">
					<div id="header-content">
						<span id="header-logo">613 583-0835</span>
						<!-- // TODO: check for CSS3. <IE9, <Firefox 4, <Safari 5. then display a warning box, that then displays browser warning, on click -->
						<div id="nav">
						<li><?php echo '<a href="'.base_url('/rentals').'">Rentals</a>'; ?></li>
						<li><?php echo '<a href="'.base_url('/contact').'">About Us</a>'; ?></li>
						</div>
					</div>
				</div>
				<div id="header-border"></div>
			</div>
			<div id="body-content">
			<?php $this->load->view ($template . ".view.php", $data); ?>
			</div>
			<?php $this->load->view ($template . '.templates.inc'); ?>
			<div id="footer">
				<div id="copy">
					<span>&copy; 2014 Advantage Property Management</span>
				</div>
				<div id="nav">
					<ul><?php if ($admin): $this->load->view('footer.view.php'); else: ?>
						<li><?php echo '<a href="'.base_url('/login').'">Admin</a>'; ?></li>
						<?php endif; ?>
						<li><?php
						if ($template == 'page')
							echo '<a href="'.base_url('/rentals').'">Rentals</a>';
						else
							echo '<a href="'.base_url('/contact').'">Contact Us</a>';
						?></li>
					</ul>
				</div>
			</div>
			<script type="text/javascript"><?php $this->load->view ('global.script.js'); ?></script>
			<script type="text/javascript"><?php $this->load->view ($template . '.script.js'); ?></script><?php if ($admin): ?>
			<script type="text/javascript"><?php $this->load->view ($template . '.admin.script.js'); ?></script>
			<?php $this->load->view ($template . '.admin.templates.inc'); ?>
			<?php endif; ?>
		</div>
	</body>
</html>