<script id="ajaxLoading" type="text/x-jquery-tmpl">
	<table cellpadding="0" cellspacing="0" width="100%" style="height:100%;">
		<tr>
			<td align="center" valighn="middle">
				<img src="<?php echo base_url ('images/ajax-loader.gif') ?>" alt="Loading" />
			</td>
		</tr>
	</table>
</script>
<script id="ajaxSuccess" type="text/x-jquery-tmpl">
	<table cellpadding="0" cellspacing="0" width="100%" style="height:100%;">
		<tr>
			<td align="center" valighn="middle">Success!</td>
		</tr>
	</table>
</script>
<script id="ajaxFailed" type="text/x-jquery-tmpl">
<table cellpadding="0" cellspacing="0" width="100%" style="height:100%;">
	<tr>
		<td align="center" valighn="middle">Failed.</td>
	</tr>
	<tr>
		<td align="center" valighn="middle">${reason}</td>
	</tr>
</table>
</script>

