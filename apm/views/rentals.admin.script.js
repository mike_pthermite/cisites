var geocoder;
var marker;
var adminMap;
var myOptions = {
	zoom: 15
	, center: new google.maps.LatLng (44.23142, -76.48101)
	, mapTypeControl: false
	, mapTypeControlOptions: {
		style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
	}
	, navigationControl: false
	, streetViewControl: false
	, mapTypeId: google.maps.MapTypeId.ROADMAP
};

function getLatLng (event)
{
	if (event)
		event.preventDefault ();
	if (!geocoder)
		geocoder = new google.maps.Geocoder ();
	if (marker)
		marker.setMap (null);

	address =
			  $( "#addressNumber" ).val () + '  ' + $( "#addressName" ).val () + ', ' +
			  $( "#addressCity" ).val () + ', Ontario' +
			  ', Canada ';

	geocoder.geocode ({ 'address': address}, function (results, status)
	{
		if (status === google.maps.GeocoderStatus.OK)
		{
			if (status !== google.maps.GeocoderStatus.ZERO_RESULTS)
			{
				adminMap.setCenter (results[0].geometry.location);

				$( "#addressLat" ).val (results[0].geometry.location.lat ());
				$( "#addressLng" ).val (results[0].geometry.location.lng ());

				marker = new google.maps.Marker ({
					position: results[0].geometry.location
					, map: adminMap
					, title: address
				});
			} else {
				console.error ("No results found");
			}
		} else {
			console.error ("Geocode was not successful for the following reason: " + status);
		}
	});
};

$(document).ready (function ()  {
	var admin = {
		url : {
			 list: url + "adminrentals/glist"
			 , save: url +"adminrentals/save"
			 , create: url +"adminrentals/create"
			 , get: url +"adminrentals/get"
			 , delete: url + "adminrentals/delete"
			 , toggle: url + "adminrentals/toggle"
			 , thumbnail: url + "adminrentals/thumbnail"
			 , slideshowAdd: url + "adminrentals/addSlideshow"
			 , slideshowDel: url + "adminrentals/delSlideshow"
			 , roomUpdate: url + "adminrentals/updateRoom"
			 , roomDel: url + "adminrentals/delRoom"
		}
		, template: {
			rental: $("#adminRentalTemplate")
			, listRentals: $("#adminListRentalsTemplate")
			, room: $("#adminRoomTemplate")
			, slideshow: $("#adminSlideshowTemplate")
			, address: $("#adminAddressTemplate")
			, thumbnail: $("#adminThumbnailTemplate")
			, utilities: $("#adminUtilitiesTemplate")
			, error: $("#adminErrorTemplate")
		}
		, dialog:  null
		, edit : {
			isNew: false
			, id: -1
		}
	};
	admin ['load'] =  {
		get: function (buttons, title, url, callback)
					{
						admin.dialog.html (template.toHTML (template.ajaxLoading));
						admin.dialog.dialog ("option", {
							buttons: {}
							, title: 'Loading'
						});
						$.getJSON (url, function (res)
						{
							if (res.isError)
							{
								admin.dialog.html (template.toHTML (admin.template.error, res));
								admin.dialog.dialog ("option", {
									buttons: admin.buttons.error
									, title: 'ERROR'
								});
								return;
							}
							admin.dialog.dialog ("option", {
								buttons: buttons
								, title:title
							});
							console.log (url, res);
							callback (res);
						 });
					}
		, post: function (buttons, title, url, data, callback)
					{
						admin.dialog.html (template.toHTML (template.ajaxLoading));
						admin.dialog.dialog ("option", {
							buttons: {}
							, title:  'Loading'
						});
						$.post (url, data).done (function (res)
						{
							if (res.error)
							{
								admin.dialog.html (template.toHTML (admin.template.error, res));
								admin.dialog.dialog ("option", {
									buttons: admin.buttons.error
									, title: 'ERROR'
								});
								return;
							}
							admin.dialog.dialog ("option", {
								buttons: buttons
								, title: title
							});
							console.log (url, data, res);
							callback (res);
						});
					}
		, postBasic: function (url, data, callback)
					{
						$.post (url, data).done (function (res)
						{
							console.log (url, data, res);
							callback (res);
						});
					}
		, upload: function (fileName, url, data, callback, params)
					{
						$( fileName ).liteUploader (
						{
							script: url
							, allowedFileTypes: 'image/jpeg,image/png,image/gif'
							, maxSizeInBytes: 101072
							, customParams: data
							, getParams: function (customParams)
							{
								if (params)
									return params (customParams);
								return customParams;
							}
							, before: function (files)
							{
								$('#details').empty();
								$('#response').html('Uploading ' + files.length + ' file(s)...');
							}
							, each: function (file, errors)
							{
								var i, errorsDisp = '';

								if (errors.length > 0)
								{
									$('#response').html('One or more files did not pass validation');

									$.each(errors, function(i, error)
									{
										errorsDisp += '<br /><span class="error">' + error.type + ' error - Rule: ' + error.rule + '</span>';
									});
								}

								$('#details').append('<p>name: ' + file.name + ', type: ' + file.type + ', size:' + file.size + errorsDisp + '</p>');
							}
							, success: function (response)
							{
								$('#details, #response').empty();
								console.log (url, data, response);
								callback (response);
							}
						});
					}
		, main: function ()
					{
						admin.load.get (
							admin.buttons.main
							 , 'Rentals Admin - Listings'
							 , admin.url.list
							 , admin.display.main
						);
					}
		, newRental: function ()
					{
						admin.load.get (
							admin.buttons.rental
							 , 'Rentals Admin - Loading'
							 , admin.url.create
							 , admin.display.rental
						);

					}
		, editRental: function (id)
					{
						admin.load.post (
							admin.buttons.rental
							 , 'Rentals Admin - Loading'
							 , admin.url.get
							 , {  'ID': id }
							 , admin.display.rental
						);
					}
		, delete: function (id)
					{
						admin.load.post (
							admin.buttons.main
							 , 'Rentals Admin - Loading'
							 , admin.url.delete
							 , {  'ID': id }
							 , admin.display.main
						);
					}
		, toggle: function (id)
					{
						admin.load.post (
							admin.buttons.main
							 , 'Rentals Admin - Loading'
							 , admin.url.toggle
							 , {  'ID': id }
							 , admin.display.main
						);
					}
		, save: function ()
					{
						admin.load.post (
							admin.buttons.main
							 , 'Rentals Admin - Loading'
							 , admin.url.save
							 , {
								 ID: admin.edit.id
								,  general: {
									 AVAILABLE_BY: $( "#generalAvailable" ).val ()
									,  PRICE: parseInt ($( "#generalPrice" ).val ())
									,  DESCRPITION: $( "#generalDescription" ).val ()
									,  LEASE_LENGTH: parseInt ($( "#generalLeaseLength" ).val ())
									,  TYPE: $( "#generalRentalType" ).val ()
									,  BIG_ADVERT: $( "#generalBigAdvert" ).is(':checked')
									,  VISIBLE: $( "#generalVisible" ).is(':checked')
								 }
								 , UTILITIES: {
									 HEAT: $( "#utilitiesHeat" ).is(':checked')
									,  INTERNET: $( "#utilitiesInternet" ).is(':checked')
									,  LAUNDRY: $( "#utilitiesLaundry" ).is(':checked')
									,  PARKING: $( "#utilitiesParking" ).is(':checked')
									,  PETS: $( "#utilitiesPets" ).is(':checked')
									,  POWER: $( "#utilitiesPower" ).is(':checked')
									,  SMOKING: $( "#utilitiesSmoking" ).is(':checked')
									,  WATER: $( "#utilitiesWater" ).is(':checked')
								 }
								, ADDRESS: {
									 CITY: $( "#addressCity" ).val ()
									,  HOUSE_NUMBER: parseInt ($( "#addressNumber" ).val ())
									,  Lat: parseFloat ($( "#addressLat" ).val ())
									,  Lng: parseFloat ($( "#addressLng" ).val ())
									,  PROVINCE: $( "#PROVINCE" ).val ()
									,  STREET: $( "#addressName" ).val ()
								 }
							 }
							 , admin.display.main
						);
					}
	};
	admin ['buttons'] = {
		main:  {
			'New': admin.load.newRental
		}
		, rental:  {
			'< Cancel': function () {
				if (admin.edit.isNew)
					admin.load.delete (admin.edit.id);
				else
					admin.load.main ();
			}
			, 'Save':  admin.load.save
		}
		, error:  {
			'Back': admin.load.main
		}
	};
	admin ['display'] =  {
		rental: function (data)
					{
						function linkSlideShow (img)
						{
							$( "#admSlideshowContainer" ).append (template.toHTML (admin.template.slideshow, img));
							$( "#slideshowDelete_" + img ['ID'] ).button ({
								icons: {
									primary: "ui-icon-trash"
								}
							});
							$( "#slideshowDelete_" + img ['ID'] ).click (function (event)
							{
								admin.load.postBasic (admin.url.slideshowDel
									, {
										ID: data ['ID']
										, img: img ['ID']
									}
									, function (res)
									{
										$( "#slideshow_" + img ['ID'] ).remove ();
									}
								);
							});
						};
						var hasNewRoom = false;
						function linkRoom (room)
						{
							function enableSave (event)
							{
								$( "#roomSave_" + room ['ID'] ).button ("enable");
							}
							if (hasNewRoom)
								$( "#room_" + room ['ID'] ).remove ();

							$( "#admRoomsContainer" ).append (template.toHTML (admin.template.room, room));
							$( "#roomDelete_" + room ['ID'] ).button ({ icons: { primary: "ui-icon-trash" }, text: false });
							$( "#roomDelete_" + room ['ID'] ).click (function (event)
							{
								if (room ['ID'] === 'new')
									$( "#room_" + room ['ID'] ).remove ();
								else
									admin.load.postBasic (admin.url.roomDel
										, {
											ID: data ['ID']
											, 'room': room ['ID']
										}
										, function (res)
										{
											if (res ['success'])
												$( "#room_" + room ['ID'] ).remove ();
										}
									);

							});
							$( "#roomSave_" + room ['ID'] ).button ({ icons: { primary: "ui-icon-disk" } });
							$( "#roomSave_" + room ['ID'] ).button ("disable");
							$( "#roomName_" + room ['ID'] ).change (enableSave);
							$( "#roomSizeX_" + room ['ID'] ).change (enableSave);
							$( "#roomSizeY_" + room ['ID'] ).change (enableSave);

							$( "#roomType_" + room ['ID'] ).change (enableSave);
							$( "#roomType_" + room ['ID'] ).val (room ['ROOM_TYPE']);
							$( "#roomType_" + room ['ID'] ).combobox ();

							$( "#roomSave_" + room ['ID'] ).click (function (event)
							{
								admin.load.postBasic (admin.url.roomUpdate
									, {
										ID: data ['ID']
										, 'room': {
											ID: room ['ID']
											, ROOM_NAME: $( "#roomName_" + room ['ID'] ).val ()
											, ROOM_TYPE: $( "#roomType_" + room ['ID'] ).val ()
											, DIMENSION_1: $( "#roomSizeX_" + room ['ID'] ).val ()
											, DIMENSION_2: $( "#roomSizeY_" + room ['ID'] ).val ()
										}
									}
									, function (res)
									{
										if (room ['ID'] === 'new')
											hasNewRoom = false;

										$( "#room_" + room ['ID'] ).remove ();
										linkRoom (res);
									}
								);
							});
							if (room ['ID'] === 'new')
								hasNewRoom = true;
						};
						admin.edit.isNew = data ['isNew'];
						admin.edit.id = data ['ID'];

						if (admin.edit.isNew)
							admin.dialog.dialog ("option", { title: 'Rentals Admin - Creatitng New #' + admin.edit.id });
						else
							admin.dialog.dialog ("option", { title: 'Rentals Admin - Editing #' + admin.edit.id });

						admin.dialog.html (template.toHTML (admin.template.rental, data));
						$( "#admUtilitiesContainer" ).html (template.toHTML (admin.template.utilities, data ['UTILITIES']));
						$( "#admAddressContainer" ).html (template.toHTML (admin.template.address, data ['ADDRESS']));
						$( "#admThumbnailContainer" ).html (template.toHTML (admin.template.thumbnail, data ['THUMBNAIL']));

						//Aloha.ready (function () {
						//	$( '#generalDescription' ).aloha ();
						//});
						new wysihtml5.Editor ("generalDescription", {
							//toolbar:      "toolbar",
							parserRules:  wysihtml5ParserRules
						});

						$( "#admRoomsContainer").html ('');
						for (ROOMS = 0; ROOMS < data['ROOMS'].length; ++ROOMS)
							linkRoom (data ['ROOMS'] [ROOMS]);

						$( "#admSlideshowContainer").html ('');
						for (IMAGES = 0; IMAGES < data['IMAGES'].length; ++IMAGES)
							linkSlideShow (data ['IMAGES'] [IMAGES]);


						$( '#generalRentalType' ).val (data ['TYPE']);
						$( "#generalRentalType" ).combobox ();
						$( "#generalLeaseLength" ).spinner ({ min: 1 });
						$( "#generalAvailable" ).datepicker ({ changeMonth: true , changeYear: true , dateFormat:'yy-mm-dd'});
						$( "#generalChecks" ).buttonset ();

						$( "#utilitiesParking" ).button ({ icons: { primary: "ui-icon-utilities-parking" } });
						$( "#utilitiesPower" ).button ({ icons: { primary: "ui-icon-utilities-power" } });
						$( "#utilitiesHeat" ).button ({ icons: { primary: "ui-icon-utilities-heat" } });
						$( "#utilitiesWater" ).button ({ icons: { primary: "ui-icon-utilities-water" } });
						$( "#utilitiesGas" ).button ({ icons: { primary: "ui-icon-utilities-gas" } });
						$( "#utilitiesGasAvailable" ).button ({ icons: { primary: "ui-icon-utilities-gas-available" } });
						$( "#utilitiesLaundry" ).button ({ icons: { primary: "ui-icon-utilities-laundry" } });
						$( "#utilitiesInternet" ).button ({ icons: { primary: "ui-icon-utilities-internet" } });
						$( "#utilitiesSmoking" ).button ({ icons: { primary: "ui-icon-utilities-smoking" } });
						$( "#utilitiesPets" ).button ({ icons: { primary: "ui-icon-utilities-pets" } });

						$( "#findAddress" ).button ({ icons: { primary: "ui-icon-search" } }).click (getLatLng);
						$( "#addressProvince" ).combobox ();

						$( "#roomsAdd" ).button ({ icons: { primary: "ui-icon-plus" } });
						$( "#roomsAdd" ).click (function (event) {
							linkRoom ({
								ID: 'new'
							});
						});

						$( "#picturesChangeThumbnail" ).button ({ icons: { primary: "ui-icon-pencil" } });
						admin.load.upload (
							"#picturesChangeThumbnailFile"
							, admin.url.thumbnail
							, {
								ID: admin.edit.id
							}
							, function (res) {
								$('#thumbnailImg').attr ("src", url + res['LINK']);
							}
						);
						$( "#picturesAddSlideShow" ).button ({ icons: { primary: "ui-icon-folder-open" } });

						admin.load.upload (
							"#picturesAddSlideShowFile"
							, admin.url.slideshowAdd
							, {}
							, function (res) {
								$('#slideShowCaption').val ('');
								linkSlideShow (res);
							}
							, function (oldparams)
							{
								return {
									ID: admin.edit.id
									, CAPTION: $('#slideShowCaption').val ()
								};
							}
						);



						adminMap = new google.maps.Map (document.getElementById ("addressMap"), myOptions);
						getLatLng (null);

						//$( "#editRentalAccordion" ).accordion ({ heightStyle: "fill" });
					}
		, main: function (data)
					{
						admin.dialog.html  ('<div id="adminListings"></div>');
						var listings = $('#adminListings');

						displayListing ('visible');
						displayListing ('hidden');

						var btnN = 'quickEditBtn' +  parseInt (' '+ (Math.random ()*100));
						var firstID = 0;
						if (data ['visible'].length > 0)
							firstID = data ['visible'] [0] ['ID'];
						$('.ui-dialog-buttonset').append ('| <input id="quickEditNum" type="text" class="ui-corner-all ui-widget-content" size="4" placeholder="'
							+ firstID + '" />');
						$('.ui-dialog-buttonset').append ('<button id="'+btnN+'">Edit</button>');

						$( '#'+btnN).button ({ icons: { primary: "ui-icon-pencil" } });
						$( '#'+btnN).click ( function (event)
						{
							event.preventDefault ();
							var id = parseInt ($( "#quickEditNum" ).val ());
							admin.load.editRental (isNaN (id) ? firstID : id);
						});
						function displayListing (arrayName)
						{
							for (index = 0; index < data [arrayName].length; ++index)
								showListing (data [arrayName] [index]);

						}
						function showListing (rental)
						{
							listings.append (template.toHTML (admin.template.listRentals, {
								ID: rental ['ID']
								, VISIBLE: rental ['VISIBLE']
								, TITLE: rental ['address'] ['HOUSE_NUMBER'] + ' ' + rental ['address'] ['STREET'] + ' ($'  + rental ['PRICE'] + ')' + ' ('  + rental ['countBed'] + ' Bedrooms)'
							}));
							$( "#del" + rental ['ID']).button ({ icons: { primary: "ui-icon-trash" }, text: false });
							if (rental ['VISIBLE'])
								$( "#toggle" + rental ['ID'] ).button ({ icons: { primary: "ui-icon-bullet" }, text: false });
							else
								$( "#toggle" + rental ['ID'] ).button ({ icons: { primary: "ui-icon-radio-off" }, text: false });
							$( "#toggle" + rental ['ID'] ).click ( function (event)
							{
								event.preventDefault ();
								admin.load.toggle (rental ['ID']);
							});
							$( "#del" + rental ['ID'] ).click ( function (event)
							{
								event.preventDefault ();
								admin.load.delete (rental ['ID']);
							});
							$( "#edit" + rental ['ID'] ).click ( function (event)
							{
								event.preventDefault ();
								admin.load.editRental (rental ['ID']);
							});
						}
					}
	};
	$('#admin').click (function (event) {
		event.preventDefault ();
		dialog.create (
			template.toHTML (template.ajaxLoading)
			, {
				title: 'Rentals Admin'
				, autoOpen: true
				, height: 550
				, width: 550
				, modal: true
				, buttons: admin.buttons.main
				, close: function () {
					window.location  = window.location + '';
				}
				, create: function (event, ui) {
					admin.dialog = $(this);
					admin.load.main ();
				}
			}
		);
	});
});