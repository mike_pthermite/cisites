<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MY_Controller {

	public function __construct()
		{ parent::__construct(); }
	public function view ($id)
	{
		$this->render ($this->page ($id)->toArray(),'page');
	}
}